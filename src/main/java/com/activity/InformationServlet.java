package com.activity;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class InformationServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<h1>You are now using the Calculator App</h1>");
		out.println("<h3>To use the app, input two numbers and an operation.</h3>");
		out.println("<h3>Hit the submit/calculate button after filling in the details.</h3>");
		out.println("<h3>You will get the results shown in your browser.</h3>");
		out.println("To go back to main page <a href=index.html> Click Here </a>");
		}
}






